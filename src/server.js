import express from 'express';
import config from './config';
import proxy from './proxy';

const app = express();
const PORT = process.env.PORT || 3000;
const ROUTE = '/:category/:name.:extension?';

app.get(ROUTE, config, proxy);
app.listen(PORT, () => console.log(`Listening on ${PORT}`));
