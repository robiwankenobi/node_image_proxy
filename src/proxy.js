import { contains } from 'ramda';
import headers from './headers';
import { resize, saveToFile } from './image';
import { request, deliver, deny, deliverCompressed } from './utils';

const ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'png'];

export default (req, res) => {
  if (!contains(req.params.extension, ALLOWED_EXTENSIONS)) return deny(req, res, 403);
  getRequestedImage(req, res)
    .catch(() => {
      getImageFromOriginal(req, res)
        .catch(error => {
          // TODO: add fallback image
          return deny(req, res, 404);
        });
    });
};

const getRequestedImage = (req, res) => request(req.params.targetUrl, req)
  .then((response) => {
    headers(response.headers, res);
    req.params.compressed ? deliverCompressed(req, res, response.data) : deliver(req, res, response.data);
  });

const getImageFromOriginal = (req, res) => request(req.params.originalImageUrl, req)
  .then(response => {
    headers(response.headers, res);
    resize(response.data, { sizes: req.params.sizes })
      .then(resizedImage => {
        const { info, data } = resizedImage;
        res.setHeader('content-length', info.size);
        req.params.compressed ? deliverCompressed(req, res, data) : deliver(req, res, data);
        saveToFile(req, res, data);
      })
      .catch(err => deny(req, res, 404));
  });
