import { has, head, replace, split } from 'ramda';

const TARGET_SERVER = 'http://localhost:8888';
const DEFAULT_QUALITY = 80;

export default (req, res, next) => {
  const url = req.url;
  const resizedImage = url.match(/@+\d+x\d+/);
  if (resizedImage) {
    const sizeString = head(resizedImage);
    req.params.originalImageUrl = `${TARGET_SERVER}${replace(sizeString, '', url)}`;
    req.params.sizes = split('x', replace('@', '', sizeString));
  }

  req.params.targetUrl = `${TARGET_SERVER}${url}`;
  req.params.compressed = has('q', req.query);
  req.params.quality = parseInt(req.query.q) || DEFAULT_QUALITY;

  next();
};
