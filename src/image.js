import sharp from 'sharp';
import path from 'path';

export const resize = (image, options) => {
  return sharp(image)
    .resize(parseInt(options.sizes[0]), parseInt(options.sizes[1]))
    .toBuffer({ 'resolveWithObject': true });
};


// TODO: compress image if needed...?
export const compress = (image, options) => {
  const format = options.format;
  return sharp(image)
    .toFormat(format, {
      quality: options.quality,
      progressive: true,
      compressScans: true
    })
    .toBuffer({ 'resolveWithObject': true });
};

// TODO: save new format to bucket. experimental:
// TODO: extract options
export const saveToFile = (req, res, image) => {
  return sharp(image)
    .toFile(path.resolve(__dirname, `../demo/products/${req.params.name}.${req.params.extension}`));
};
