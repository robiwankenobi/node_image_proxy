import axios from 'axios';
import { compress } from './image';

export const request = (url, req) => {
  return axios.get(url, {
    headers: {'x-forwarded-for': req.ip},
    responseType: 'arraybuffer'
  });
};

export const deliver = (req, res, data) => {
  // TODO: set response data size header
  // TODO: Cache header
  // res.setHeader('content-length', data.length);
  res.status(200);
  res.write(data);
  res.end();
};

export const deliverCompressed = (req, res, data) => {
  compress(data, { format: req.params.extension, quality: req.params.quality })
    .then(compressedImage => {
      const { info, data } = compressedImage;
      res.setHeader('content-length', info.size);
      deliver(req, res, data);
    })
    .catch(err => deny(req, res, 404));
};

export const deny = (req, res, error) => {
  res.setHeader('content-length', 0);
  res.removeHeader('cache-control');
  res.removeHeader('expires');
  res.removeHeader('date');
  res.status(error || 404);
  res.end();
};
