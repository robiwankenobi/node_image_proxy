export default (source, target) => {
  // copy proxy response headers to client response
  for (const [key, value] of Object.entries(source)) {
    try {
      target.setHeader(key, value);
    } catch (e) {
      console.log(e.message);
    }
  };
  target.setHeader('content-encoding', 'identity');
};
