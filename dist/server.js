module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./dist";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/config.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ramda__ = __webpack_require__("ramda");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ramda___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ramda__);


const TARGET_SERVER = 'http://localhost:8888';
const DEFAULT_QUALITY = 80;

/* harmony default export */ __webpack_exports__["a"] = ((req, res, next) => {
  const url = req.url;
  const resizedImage = url.match(/@+\d+x\d+/);
  if (resizedImage) {
    const sizeString = Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["head"])(resizedImage);
    req.params.originalImageUrl = `${TARGET_SERVER}${Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["replace"])(sizeString, '', url)}`;
    req.params.sizes = Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["split"])('x', Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["replace"])('@', '', sizeString));
  }

  req.params.targetUrl = `${TARGET_SERVER}${url}`;
  req.params.compressed = Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["has"])('q', req.query);
  req.params.quality = parseInt(req.query.q) || DEFAULT_QUALITY;

  next();
});

/***/ }),

/***/ "./src/headers.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ((source, target) => {
  // copy proxy response headers to client response
  for (const [key, value] of Object.entries(source)) {
    try {
      target.setHeader(key, value);
    } catch (e) {
      console.log(e.message);
    }
  };
  target.setHeader('content-encoding', 'identity');
});

/***/ }),

/***/ "./src/image.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sharp__ = __webpack_require__("sharp");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sharp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sharp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_path__ = __webpack_require__("path");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_path___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_path__);



const resize = (image, options) => {
  return __WEBPACK_IMPORTED_MODULE_0_sharp___default()(image).resize(parseInt(options.sizes[0]), parseInt(options.sizes[1])).toBuffer({ 'resolveWithObject': true });
};
/* harmony export (immutable) */ __webpack_exports__["b"] = resize;


// TODO: compress image if needed...?
const compress = (image, options) => {
  const format = options.format;
  return __WEBPACK_IMPORTED_MODULE_0_sharp___default()(image).toFormat(format, {
    quality: options.quality,
    progressive: true,
    compressScans: true
  }).toBuffer({ 'resolveWithObject': true });
};
/* harmony export (immutable) */ __webpack_exports__["a"] = compress;


// TODO: save new format to bucket. experimental:
// TODO: extract options
const saveToFile = (req, res, image) => {
  return __WEBPACK_IMPORTED_MODULE_0_sharp___default()(image).toFile(__WEBPACK_IMPORTED_MODULE_1_path___default.a.resolve(__dirname, `../demo/products/${req.params.name}.${req.params.extension}`));
};
/* harmony export (immutable) */ __webpack_exports__["c"] = saveToFile;


/***/ }),

/***/ "./src/proxy.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ramda__ = __webpack_require__("ramda");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ramda___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_ramda__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__headers__ = __webpack_require__("./src/headers.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__image__ = __webpack_require__("./src/image.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils__ = __webpack_require__("./src/utils.js");





const ALLOWED_EXTENSIONS = ['jpg', 'jpeg', 'png'];

/* harmony default export */ __webpack_exports__["a"] = ((req, res) => {
  if (!Object(__WEBPACK_IMPORTED_MODULE_0_ramda__["contains"])(req.params.extension, ALLOWED_EXTENSIONS)) return Object(__WEBPACK_IMPORTED_MODULE_3__utils__["c" /* deny */])(req, res, 403);
  getRequestedImage(req, res).catch(() => {
    getImageFromOriginal(req, res).catch(error => {
      // TODO: add fallback image
      return Object(__WEBPACK_IMPORTED_MODULE_3__utils__["c" /* deny */])(req, res, 404);
    });
  });
});

const getRequestedImage = (req, res) => Object(__WEBPACK_IMPORTED_MODULE_3__utils__["d" /* request */])(req.params.targetUrl, req).then(response => {
  Object(__WEBPACK_IMPORTED_MODULE_1__headers__["a" /* default */])(response.headers, res);
  req.params.compressed ? Object(__WEBPACK_IMPORTED_MODULE_3__utils__["b" /* deliverCompressed */])(req, res, response.data) : Object(__WEBPACK_IMPORTED_MODULE_3__utils__["a" /* deliver */])(req, res, response.data);
});

const getImageFromOriginal = (req, res) => Object(__WEBPACK_IMPORTED_MODULE_3__utils__["d" /* request */])(req.params.originalImageUrl, req).then(response => {
  Object(__WEBPACK_IMPORTED_MODULE_1__headers__["a" /* default */])(response.headers, res);
  Object(__WEBPACK_IMPORTED_MODULE_2__image__["b" /* resize */])(response.data, { sizes: req.params.sizes }).then(resizedImage => {
    const { info, data } = resizedImage;
    res.setHeader('content-length', info.size);
    req.params.compressed ? Object(__WEBPACK_IMPORTED_MODULE_3__utils__["b" /* deliverCompressed */])(req, res, data) : Object(__WEBPACK_IMPORTED_MODULE_3__utils__["a" /* deliver */])(req, res, data);
    Object(__WEBPACK_IMPORTED_MODULE_2__image__["c" /* saveToFile */])(req, res, data);
  }).catch(err => Object(__WEBPACK_IMPORTED_MODULE_3__utils__["c" /* deny */])(req, res, 404));
});

/***/ }),

/***/ "./src/server.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express__ = __webpack_require__("express");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_express___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_express__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__config__ = __webpack_require__("./src/config.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__proxy__ = __webpack_require__("./src/proxy.js");




const app = __WEBPACK_IMPORTED_MODULE_0_express___default()();
const PORT = Object({"NODE_ENV":undefined}).PORT || 3000;
const ROUTE = '/:category/:name.:extension?';

app.get(ROUTE, __WEBPACK_IMPORTED_MODULE_1__config__["a" /* default */], __WEBPACK_IMPORTED_MODULE_2__proxy__["a" /* default */]);
app.listen(PORT, () => console.log(`Listening on ${PORT}`));

/***/ }),

/***/ "./src/utils.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__("axios");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__image__ = __webpack_require__("./src/image.js");



const request = (url, req) => {
  return __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get(url, {
    headers: { 'x-forwarded-for': req.ip },
    responseType: 'arraybuffer'
  });
};
/* harmony export (immutable) */ __webpack_exports__["d"] = request;


const deliver = (req, res, data) => {
  // TODO: set response data size header
  // TODO: Cache header
  // res.setHeader('content-length', data.length);
  res.status(200);
  res.write(data);
  res.end();
};
/* harmony export (immutable) */ __webpack_exports__["a"] = deliver;


const deliverCompressed = (req, res, data) => {
  Object(__WEBPACK_IMPORTED_MODULE_1__image__["a" /* compress */])(data, { format: req.params.extension, quality: req.params.quality }).then(compressedImage => {
    const { info, data } = compressedImage;
    res.setHeader('content-length', info.size);
    deliver(req, res, data);
  }).catch(err => deny(req, res, 404));
};
/* harmony export (immutable) */ __webpack_exports__["b"] = deliverCompressed;


const deny = (req, res, error) => {
  res.setHeader('content-length', 0);
  res.removeHeader('cache-control');
  res.removeHeader('expires');
  res.removeHeader('date');
  res.status(error || 404);
  res.end();
};
/* harmony export (immutable) */ __webpack_exports__["c"] = deny;


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("babel-polyfill");
module.exports = __webpack_require__("./src/server.js");


/***/ }),

/***/ "axios":
/***/ (function(module, exports) {

module.exports = require("axios");

/***/ }),

/***/ "babel-polyfill":
/***/ (function(module, exports) {

module.exports = require("babel-polyfill");

/***/ }),

/***/ "express":
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "path":
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "ramda":
/***/ (function(module, exports) {

module.exports = require("ramda");

/***/ }),

/***/ "sharp":
/***/ (function(module, exports) {

module.exports = require("sharp");

/***/ })

/******/ });